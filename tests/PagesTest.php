<?php

use Guzzle\Http\Client;

class PagesTest extends \Guzzle\Tests\GuzzleTestCase
{

    public function testHeaders()
    {
        $client = new Client('http://matsmart.qa.rolique.space');
        $this->assertEquals('http://matsmart.qa.rolique.space', $client->getBaseUrl());

        $request = $client->get("/api/v1.0/pages");

        $response = $request->send();

        $this->assertTrue($response->getHeader('server')->hasValue("nginx"));
        $this->assertTrue($response->getHeader('content-type')->hasValue("application/json; charset=utf-8"));
        $this->assertTrue($response->getHeader('Transfer-Encoding')->hasValue("chunked"));
        $this->assertTrue($response->getHeader('connection')->hasValue("keep-alive"));
        $this->assertTrue($response->getHeader('keep-alive')->hasValue("timeout=20"));
        $this->assertTrue($response->getHeader('vary')->hasValue("Accept-Encoding"));
        $this->assertTrue($response->getHeader('x-powered-by')->hasValue("PHP/5.5.9-1ubuntu4.17"));
        $this->assertTrue($response->getHeader('cache-control')->hasValue("no-cache, must-revalidate, post-check=0, pre-check=0"));
        $this->assertTrue($response->getHeader('x-api-version')->hasValue("v1.0"));
        $this->assertEquals("200", $response->getInfo("http_code"));

        $json = $response->json();

        $data = $json["data"];

        $keys = array();
        $values = array();
        foreach ($data as $key => $value) {
            array_push($keys, $value["id"]);
            array_push($values, $value);
        }

        $formedData = array_combine($keys, $values);

        return $formedData;
    }

    /**
     * @depends testHeaders
     */
    public function testPresentPages(array $formedData)
    {
        $ids = array(206, 270, 278, 279, 280, 281, 282, 283, 284, 285);

        foreach ($ids as $key => $value) {
            $this->assertTrue(array_key_exists($value, $formedData));
        }
    }

    /**
     * @depends testHeaders
     */
    public function test206Node(array $formedData)
    {
        $this->assertEquals($formedData[206]["id"], 206);
        $this->assertEquals($formedData[206]["title"], "Kontakt");
        $this->assertEquals($formedData[206]["alias"], "node/206");
    }

    /**
     * @depends testHeaders
     */
    public function test270Node(array $formedData)
    {
        $this->assertEquals($formedData[270]["id"], 270);
        $this->assertEquals($formedData[270]["title"], "Cookies");
        $this->assertEquals($formedData[270]["alias"], "node/270");
    }

    /**
     * @depends testHeaders
     */
    public function test278Node(array $formedData)
    {
        $this->assertEquals($formedData[278]["id"], 278);
        $this->assertEquals($formedData[278]["title"], "Vad är Matsmart?");
        $this->assertEquals($formedData[278]["alias"], "node/278");
    }

    /**
     * @depends testHeaders
     */
    public function test279Node(array $formedData)
    {
        $this->assertEquals($formedData[279]["id"], 279);
        $this->assertEquals($formedData[279]["title"], "Om Bäst-före-datum");
        $this->assertEquals($formedData[279]["alias"], "node/279");
    }

    /**
     * @depends testHeaders
     */
    public function test280Node(array $formedData)
    {
        $this->assertEquals($formedData[280]["id"], 280);
        $this->assertEquals($formedData[280]["title"], "Om matsvinnet");
        $this->assertEquals($formedData[280]["alias"], "node/280");
    }

    /**
     * @depends testHeaders
     */
    public function test281Node(array $formedData)
    {
        $this->assertEquals($formedData[281]["id"], 281);
        $this->assertEquals($formedData[281]["title"], "Om bolaget");
        $this->assertEquals($formedData[281]["alias"], "node/281");
    }

    /**
     * @depends testHeaders
     */
    public function test282Node(array $formedData)
    {
        $this->assertEquals($formedData[282]["id"], 282);
        $this->assertEquals($formedData[282]["title"], "Vanliga frågor och svar");
        $this->assertEquals($formedData[282]["alias"], "node/282");
    }

    /**
     * @depends testHeaders
     */
    public function test283Node(array $formedData)
    {
        $this->assertEquals($formedData[283]["id"], 283);
        $this->assertEquals($formedData[283]["title"], "Leverans och frakt");
        $this->assertEquals($formedData[283]["alias"], "node/283");
    }

    /**
     * @depends testHeaders
     */
    public function test284Node(array $formedData)
    {
        $this->assertEquals($formedData[284]["id"], 284);
        $this->assertEquals($formedData[284]["title"], "Handla och betala");
        $this->assertEquals($formedData[284]["alias"], "node/284");
    }

    /**
     * @depends testHeaders
     */
    public function test285Node(array $formedData)
    {
        $this->assertEquals($formedData[285]["id"], 285);
        $this->assertEquals($formedData[285]["title"], "Allmänna villkor");
        $this->assertEquals($formedData[285]["alias"], "node/285");
    }
}
